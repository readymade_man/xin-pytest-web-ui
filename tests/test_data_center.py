# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from base.base import BasePage
import allure
import pytest
from business.Data_Center.data_center_assert import DataCenterAssert as DC
# from PageObjects.CallCenter.callCenter_page import CallCenterPage

@allure.feature("数据中心模块")
@allure.story("测试用例数据中心")
# @pytest.mark.usefixtures("_driver")
# @pytest.mark.data_center
# @pytest.mark.skip
class TestDataCenter():

    #数据中心
    @allure.title("测试用例名称：数据中心新建功能")
    def test_my_api_life(self, _driver):
        _driver[1].my_api_new()
        # assert BasePage(self.driver).assert_text((By.XPATH, '//div[@class="notify-content"]'), doc="断言-提示")
        assert (DC(_driver[0]).data_storage_assert())

    #数据中心
    @allure.title("测试用例名称：数据中心新建功能1")
    def test_my_api_life2(self, _driver):
        _driver[1].my_api_new1()
        assert (DC(_driver[0]).data_storage_assert())
        # assert (self.assert_text(_driver[0]).importlist())

if __name__ == '__main__':
    pytest.mark(['-q', r'D:\慧测\UI自动化课件\daima\xin-pytest-web-ui\tests\test_data_center.py'])